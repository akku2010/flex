import { Component, OnInit } from "@angular/core";
import { NavController, ToastController, ViewController } from "ionic-angular";
import { NavParams } from "ionic-angular";
import { AlertController } from "ionic-angular";
import { ModalController } from "ionic-angular";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";

@Component({
    selector: 'page-update-model',
    templateUrl: './update-group.html'
})
export class UpdateGroup implements OnInit {

    islogin: any;
    GroupStatus: any = [];
    groupForm: FormGroup;
    groupDetails: any = {};
    constructor(public navCtrl: NavController, public navParams: NavParams, public apigroupupdatecall: ApiServiceProvider,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public formBuilder: FormBuilder,
        public viewCtrl: ViewController,
        private apiCall: ApiServiceProvider,
        private toastCtrl: ToastController) {

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};

        if (navParams.get('params') !== null) {
            this.groupDetails = navParams.get('params');
        }
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active",
                value: true
            },
            {
                name: "InActive",
                value: false
            }];


        this.groupForm = formBuilder.group({
            group_name: [this.groupDetails.groupname, Validators.required],
            status: [this.groupDetails.status, Validators.required],
            // grouptype: ['']
        })

    }

    ngOnInit() {

    }


    dismiss() {
        this.viewCtrl.dismiss();
    }


    UpdateGroup() {
        let url = this.apiCall.mainUrl + "groups/updateGroup";
        let payload = {
            "name": this.groupForm.value.group_name,
            "status": this.groupForm.value.status,
            "_id": this.groupDetails._id
        }

        this.apiCall.urlpasseswithdata(url, payload).subscribe((resp) => {
            console.log(resp);
            this.toastCtrl.create({
                message: 'Group details updated..!',
                duration: 2000,
                position: 'top'
            }).present();

            setTimeout(() => {
                this.viewCtrl.dismiss();
            }, 2000);
        })
    }
}